import "reflect-metadata";
import { GraphQLServer } from "graphql-yoga";
import { createConnection, getConnection } from "typeorm";

import { ResolverMap } from "./types/ResolverType";

import { User } from "./entity/User";

const typeDefs = `
  type User {
    id: Int!
    firstName: String!
    lastName: String!
    age: Int!
    email: String!
  }

  type Query {
    hello(name: String): String!
    user(id: Int!): User
    users: [User!]!
  }

  type Mutation {
    createUser(firstName: String!, lastName: String!, age: Int!, email: String): User!
    updateUser(id: Int!, firstName: String, lastName: String, age: Int, email: String): Boolean
    deleteUser(id: Int!): Boolean
  }
`;

const resolvers: ResolverMap = {
  Query: {
    hello: (_: any, { name }: any) => `hello ${name || "World"}`,
    user: (_, { id }) => User.findOne({ id }),
    users: () => User.find(),
  },

  Mutation: {
    createUser: (_, args) =>
      User.create({
        firstName: args.firstName,
        lastName: args.lastName,
        email: args.email,
        age: args.age,
      }).save(),

    updateUser: (_, { id, ...args }) => {
      try {
        User.update({ id }, { ...args });
      } catch (err) {
        console.log(err);
        return false;
      }

      return true;
    },

    deleteUser: async (_, { id }) => {
      try {
        // User.delete({ id });

        const deleteQuery = await getConnection()
          .createQueryBuilder()
          .delete()
          .from(User)
          .where("id = :id", { id });

        if (id === 8) {
          deleteQuery.andWhere("email = :email", {
            email: "kaisar.map@gmail.com",
          });
        }

        await deleteQuery.execute();
      } catch (err) {
        console.log(err);
        return false;
      }

      return true;
    },
  },
};

const server = new GraphQLServer({ typeDefs, resolvers });
createConnection().then(() => {
  server.start(() => console.log("Server is running on localhost:4000"));
});
