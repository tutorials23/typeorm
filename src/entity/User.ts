import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "text", unique: true })
  email: string;

  @Column({ type: "bool", default: false })
  confirmed: boolean;

  @Column({ type: "varchar", length: "230" })
  lastName: string;

  @Column({ type: "varchar", length: "230" })
  firstName: string;

  @Column({ type: "int" })
  age: number;
}
